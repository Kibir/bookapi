from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    """Custom permission class to allow only service owners to edit them."""

    def has_object_permission(self, request, view, obj):
        """Return True if permission is granted to the service owner."""
        if request.user:
            return obj == request.user
        return False