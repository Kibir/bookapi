from django.apps import AppConfig


class ZetaConfig(AppConfig):
    name = 'zeta'
