from rest_framework import generics
from rest_framework import permissions
from django.shortcuts import get_list_or_404
#from .permissions import IsOwner
from .serializers import ServiceSerializer, ReviewSerializer
from .models import Service, Review


class ServiceUpdateDelete(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ServiceSerializer
    lookup_url_kwarg = 'service_id'

    def get_queryset(self):
        return Service.objects.filter(creator = self.request.user)

class ReviewCreate(generics.ListCreateAPIView):
    serializer_class = ReviewSerializer

    def perform_create(self, serializer):
        serializer.save(creator =  self.request.user)

class ServiceList(generics.ListCreateAPIView):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    def get_queryset(self):
        s = self.request.query_params.get('s')
        results = Service.objects.filter(creator = self.request.user)

        if s:
            results = results.filter(title_icontains = s)
        return results

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)


# class ServiceDetails(generics.RetrieveUpdateDestroyAPIView):
#     """This class handles the http GET, PUT and DELETE requests."""
#
#     queryset = Service.objects.all()
#     serializer_class = ServiceSerializer
#     permission_classes = (permissions.IsAuthenticated, )
#
# class EmployeeList(generics.ListCreateAPIView):
#     """This class defines the create behavior of our rest api."""
#     queryset = Employee.objects.all()
#     serializer_class = EmployeeSerializer
#     permission_classes =(
#         permissions.IsAuthenticated, )
#
# class EmployeeDetails(generics.RetrieveUpdateDestroyAPIView):
#     """This class handles the http GET, PUT and DELETE requests."""
#
#     queryset = Employee.objects.all()
#     serializer_class = EmployeeSerializer
#     permission_classes = (permissions.IsAuthenticated, )
#
# class BookingList(generics.ListCreateAPIView):
#     """This class defines the create behavior of our rest api."""
#     queryset = Booking.objects.all()
#     serializer_class = BookingSerializer
#     permission_classes =(
#         permissions.IsAuthenticated, )
#
# class BookingDetails(generics.RetrieveUpdateDestroyAPIView):
#     """This class handles the http GET, PUT and DELETE requests."""
#
#     queryset = Booking.objects.all()
#     serializer_class = BookingSerializer
#     permission_classes = (permissions.IsAuthenticated, )
#
# class BusinessList(generics.ListCreateAPIView):
#     """This class defines the create behavior of our rest api."""
#     queryset = Business.objects.all()
#     serializer_class = BusinessSerializer
#     permission_classes =(
#         permissions.IsAuthenticated, )
#
#
# class BusinessDetails(generics.RetrieveUpdateDestroyAPIView):
#     """This class handles the http GET, PUT and DELETE requests."""
#
#     queryset = Business.objects.all()
#     serializer_class = BusinessSerializer
#     permission_classes = (permissions.IsAuthenticated, )
#
# class BusinessCategoryList(generics.ListCreateAPIView):
#     """This class defines the create behavior of our rest api."""
#     queryset = BusinessCategory.objects.all()
#     serializer_class = BusinessCategorySerializer
#     permission_classes =(
#         permissions.IsAuthenticated, )
#
#    # def perform_create(self, serializer):
#       #  """Save the post data when creating a new service."""
#       #  serializer.save(owner=self.request.user)
#
# class BusinessCategoryDetails(generics.RetrieveUpdateDestroyAPIView):
#     """This class handles the http GET, PUT and DELETE requests."""
#
#     queryset = BusinessCategory.objects.all()
#     serializer_class = BusinessCategorySerializer
#     permission_classes = (permissions.IsAuthenticated, )
#
# class BusinessProviderList(generics.ListCreateAPIView):
#     """This class defines the create behavior of our rest api."""
#     queryset = BusinessProvider.objects.all()
#     serializer_class = BusinessProviderSerializer
#     permission_classes =(
#         permissions.IsAuthenticated, )
#
#     def perform_create(self, serializer):
#         """Save the post data when creating a new service."""
#         serializer.save(owner=self.request.user)
#
# class BusinessProviderDetails(generics.RetrieveUpdateDestroyAPIView):
#     """This class handles the http GET, PUT and DELETE requests."""
#
#     queryset = BusinessProvider.objects.all()
#     serializer_class = BusinessProviderSerializer
#     permission_classes = (permissions.IsAuthenticated, )
#
# class BusinessProviderInfoList(generics.ListCreateAPIView):
#     """This class defines the create behavior of our rest api."""
#     queryset = BusinessProviderInfo.objects.all()
#     serializer_class = BusinessProviderInfoSerializer
#     permission_classes =(
#         permissions.IsAuthenticated, )
#
# class BusinessProviderInfoDetails(generics.RetrieveUpdateDestroyAPIView):
#     """This class handles the http GET, PUT and DELETE requests."""
#
#     queryset = BusinessProviderInfo.objects.all()
#     serializer_class = BusinessProviderInfoSerializer
#     permission_classes = (permissions.IsAuthenticated, )
#
# class EmployeeScheduleList(generics.ListCreateAPIView):
#     """This class defines the create behavior of our rest api."""
#     queryset = EmployeeSchedule.objects.all()
#     serializer_class = EmployeeScheduleSerializer
#     permission_classes =(
#         permissions.IsAuthenticated, )
#
#   #  def perform_create(self, serializer):
#       #  """Save the post data when creating a new service."""
#        # serializer.save(owner=self.request.user)
#
# class EmployeeScheduleDetails(generics.RetrieveUpdateDestroyAPIView):
#     """This class handles the http GET, PUT and DELETE requests."""
#
#     queryset = EmployeeSchedule.objects.all()
#     serializer_class = EmployeeScheduleSerializer
#     permission_classes = (permissions.IsAuthenticated,)
# """
# class UserList(generics.ListAPIView):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
#
# class UserDetails(generics.RetrieveAPIView):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
#
# """
