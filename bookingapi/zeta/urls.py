from django.conf.urls import url, include
from .views import ServiceUpdateDelete, ReviewCreate, ServiceList
urlpatterns = [
    url(r'^auth/', include('rest_framework.urls', namespace ='rest_framework')),
    ##url(r'users/$', views=UserList.as_view()),
    ##url(r'users/(?p<pk>[0-9]+)/$', views=UserDetails.as_view()),
    url(r'^services/$', ServiceList.as_view()),
    url(r'reviews/$' ,ReviewCreate.as_view()),
    # url(r'^services/(?P<pk>[0-9]+)/$', ServiceDetails.as_view(), name="details"),
    # url(r'^bookings/$', BookingList.as_view(), name="create"),
    # url(r'^businesses/$', BusinessList.as_view(), name="create"),
    # url(r'^providers/$', BusinessProviderList.as_view(), name="create"),
    # url(r'^providerinfo/$', BusinessProviderInfoList.as_view(), name="create"),
    # url(r'^employees/$', EmployeeList.as_view(), name="create"),
    # url(r'^schedules/$', EmployeeScheduleList.as_view(), name="create"),
    # url(r'^businesscat/$', BusinessCategoryList.as_view(), name="create"),
]

#urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html'])
