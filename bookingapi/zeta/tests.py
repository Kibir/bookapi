from django.test import TestCase

from .models import Service


class ModelTestCase(TestCase):
    """This class defines the test suite for the service model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.service_name = "Write world class code"
        self.service = Service(name=self.service_name)

    def test_model_can_create_a_service(self):
        """Test the service model can create a service."""
        old_count = Service.objects.count()
        self.bucketlist.save()
        new_count = Service.objects.count()
        self.assertNotEqual(old_count, new_count)