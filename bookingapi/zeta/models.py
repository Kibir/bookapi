from django.db import models
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator

class Service(models.Model):
    name = models.CharField(max_length=255, blank=False, unique=True)
    description = models.CharField(max_length = 255, blank = True)
    service_price = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    creator = models.ForeignKey(
        'api_auth.Account',
        on_delete=models.CASCADE,
        related_name='services'
    )

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.name)

class Review(models.Model):
    """ This class represents the service reviews model"""
    title = models.CharField(max_length=255)
    review = models.TextField()
    rating = models.IntegerField(default=5)
    service = models.ForeignKey(
        'Service',
        on_delete=models.CASCADE,
        related_name='reviews'
    )
# class BookingCancelation(models.Model):
#     pass
#
# class Employee(models.Model):
#     number = models.CharField(max_length=20)
#     first_name = models.CharField(max_length=100)
#     last_name = models.CharField(max_length=100)
#     hired_date = models.DateTimeField(default=timezone.now)
#
# class BusinessCategory(models.Model):
#     category_name = models.CharField(max_length=100)
#     category_description = models.CharField(max_length = 1000)
#
# class Business(models.Model):
#     category = models.ForeignKey(BusinessCategory)
#     name = models.CharField(max_length=100)
#     number = models.CharField(max_length=20)
#     address = models.CharField(max_length=1000)
#     opening_date = models.DateTimeField(default=timezone.now)
#     # Business hours in a 24 hour clock.  Default 8am-5pm.
#     business_hours_start = models.IntegerField(
#         default=8,
#         validators=[
#             MinValueValidator(0),
#             MaxValueValidator(23)
#         ]
#     )
#     business_hours_end = models.IntegerField(
#         default=17,
#         validators=[
#             MinValueValidator(0),
#             MaxValueValidator(23)
#         ]
#     )
#
# class Booking(models.Model):
#     employee = models.ForeignKey(Employee)
#     owner = models.ForeignKey('auth.User',related_name='bookings',on_delete = models.CASCADE)
#     service = models.ForeignKey(Service)
#
# class BusinessProvider(models.Model):
#     business = models.ForeignKey(Business)
#     provider_name=models.CharField(max_length =100)
#     provider_address = models.CharField(max_length =1000)
#     provider_number = models.CharField(max_length = 100)
#
# class BusinessProviderInfo(models.Model):
#     provider_description = models.CharField(max_length=1000)
#
# class EmployeeSchedule(models.Model):
#     employee = models.ForeignKey(Employee)
