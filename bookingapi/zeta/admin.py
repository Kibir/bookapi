from django.contrib import admin

# Register your models here.
from .models import *
admin.site.register(Service)
admin.site.register(Review)
# admin.site.register(Business)
# admin.site.register(Employee)
# admin.site.register(EmployeeSchedule)
# admin.site.register(BusinessProvider)
# admin.site.register(BusinessCategory)
# admin.site.register(BusinessProviderInfo)
# admin.site.register(Booking)
# admin.site.register(BookingCancelation)
