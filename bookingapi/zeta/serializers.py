from rest_framework import serializers
from django.contrib.auth import update_session_auth_hash
from api_auth.serializers import AccountSerializer
from .models import Review,Service


class ReviewSerializer(serializers.ModelSerializer):
    service = serializers.ReadOnlyField(required=False, read_only=True)

    class Meta:
        model = Review
        fields = ('id', 'title', 'review', 'rating')
        read_only_fields = ('date_created', 'date_modified')

class ServiceSerializer(serializers.ModelSerializer):
    creator = serializers.PrimaryKeyRelatedField(required=False, read_only=True)
    reviews = ReviewSerializer(many=True, read_only=True)

    class Meta:
        model = Service
        fields = ('id', 'name', 'description', 'service_price', 'reviews', 'date_created', 'date_modified', 'creator')
        # fields = ('id', 'name','description', 'service_price', 'owner', 'date_created', 'date_modified','reviews')
        read_only_fields = ('date_created', 'date_modified')


# class BookingCancelationSerializer(serializers.ModelSerializer):
#     pass
#
# class EmployeeSerializer(serializers.ModelSerializer):
#     """Serializer to map the Model instance into JSON format."""
#  #   owner = serializers.ReadOnlyField(source='owner.username')
#
#     class Meta:
#         """Meta class to map serializer's fields with the model fields."""
#         model = Employee
#         fields = ('id', 'number','first_name', 'last_name' 'hired_date')
#         read_only_fields = ('number', 'hired_date')
#
# class EmployeeScheduleSerializer(serializers.ModelSerializer):
#     """Serializer to map the Model instance into JSON format."""
#   #  owner = serializers.ReadOnlyField(source='owner.username')
#
#     class Meta:
#         """Meta class to map serializer's fields with the model fields."""
#         model = EmployeeSchedule
#         fields = ('id', 'employee')
#
# class BusinessCategorySerializer(serializers.ModelSerializer):
#     """Serializer to map the Model instance into JSON format."""
# #    owner = serializers.ReadOnlyField(source='owner.username')
#
#     class Meta:
#         """Meta class to map serializer's fields with the model fields."""
#         model = BusinessCategory
#         fields = ('id', 'category_name','category_description')
#
# class BusinessSerializer(serializers.ModelSerializer):
#     """Serializer to map the Model instance into JSON format."""
#   #  owner = serializers.ReadOnlyField(source='owner.username')
#
#     class Meta:
#         """Meta class to map serializer's fields with the model fields."""
#         model = Business
#         fields = ('id', 'name','number', 'address' 'opening_date', 'business_hours_start', 'business_hours_end')
#         read_only_fields = ('number', 'opening_date')
#
# class BookingSerializer(serializers.ModelSerializer):
#     """Serializer to map the Model instance into JSON format."""
#    # owner = serializers.ReadOnlyField(source='owner.username')
#
#     class Meta:
#         """Meta class to map serializer's fields with the model fields."""
#         model = Booking
#         fields = ('id', 'employee','owner','service')
#
# class BusinessProviderSerializer(serializers.ModelSerializer):
#     """Serializer to map the Model instance into JSON format."""
#     #owner = serializers.ReadOnlyField(source='owner.username')
#
#     class Meta:
#         """Meta class to map serializer's fields with the model fields."""
#         model = BusinessProvider
#         fields = ('id', 'business','provider_name', 'provider_address' 'provider_number')
#
# class BusinessProviderInfoSerializer(serializers.ModelSerializer):
#     """Serializer to map the Model instance into JSON format."""
#    # owner = serializers.ReadOnlyField(source='owner.username')
#
#     class Meta:
#         """Meta class to map serializer's fields with the model fields."""
#         model = BusinessProviderInfo
#         fields = ('id','provider_description')
#
# class EmployeeScheduleSerializer(serializers.ModelSerializer):
#     """Serializer to map the Model instance into JSON format."""
#    # owner = serializers.ReadOnlyField(source='owner.username')
#
#     class Meta:
#         """Meta class to map serializer's fields with the model fields."""
#         model = EmployeeSchedule
#         fields = ('id', 'employee')
#
#
# class UserSerializer(serializers.ModelSerializer):
#     services = serializers.PrimaryKeyRelatedField(many=True, queryset=Service.Object.all())
#
#     class Meta:
#         fields = ('id', 'username', 'services')
